const bcrypt = require('bcrypt-nodejs')

exports.genHash = (password, cost) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(cost, (err, salt) => {
            if (err) {
                reject(err)
            } else {
                bcrypt.hash(password, salt, null, (err, result) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(result)
                    }

                })
            }
        })
    })
}

exports.compareHash = (password, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, (err, result) => {
            if (err) {
                reject(err)
            } else {
                resolve(result)
            }
        })
        
    })
}
