'use strict'
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require("mongoose")
const hash = require('./hash')
const jwt = require('jsonwebtoken')
const cost = 10
const key = 'secret'
mongoose.connect('mongodb://localhost:27017/movie_db');
const db = mongoose.connection


db.on('error', (err) => {
    console.log(err)
});
db.once('open', () => {
    console.log("db connect")

    const checkToken = (req, res, next) => {
        let token = req.get('Authorization') || ''
        if (token.indexOf('Bearer ') === -1) {
            return res.sendStatus(401)
        }

        token = token.slice('Bearer '.length)

        try {
            const claims = jwt.verify(token, key)
            req.user = claims
            return next()
        } catch (err) {
            console.log(err)
            return res.sendStatus(401)
        }
    }

    const app = express();
    const port = 8000
    const Movie = require('./models/movie')
    const User = require('./models/user')


    const setUserMw = async (req, res, next) => {
        try {
            const user = await User
                .findOne({ email: 'pong@email.com' })
                .exec()
            req.user = user
            next()
        } catch (err) {
            console.log(err)
            res.status(500).send('on user')
        }
    }

    const favorites = [];

    // const setUser = (req, res, next) => {
    //     if (req.get('admin')) {
    //         req.user = { admin: true }
    //     }
    //     next()
    //     //res.end()
    // }

    // const onlyAdmin = (req, res, next) => {
    //     if (req.user && req.user.admin) {
    //         next()
    //     } else {
    //         res.sendStatus(401)
    //     }

    // }

    app.use(cors())

    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: false }))

    // parse application/json
    app.use(bodyParser.json())

    // app.use(setUser)
    // app.use(logMw2)
    app.use(setUserMw)

    app.get('/movies', async (req, res) => {
        console.log(req.user)
        try {
            const movies = await Movie.find().exec()
            const results = {
                results: movies
            };
            res.json(results)
        } catch (err) {
            console.log(err)
            res.status(500).send('oops! error')
        }
    });

    app.post("/signup", async (req, res) => {
        let { email, password } = req.body
        try {
            password = await hash.genHash(password, cost)
            await User.create({ email, password })
            res.sendStatus(200)
        } catch (err) {
            console.log(err)
            res.status(500).send('oops! error')
        }
    })

    app.post("/signin", async (req, res) => {
        let { email, password } = req.body
        try {
            const user = await User.findOne({ email: email }).exec()
            if (!user) {
                return res.status(401).send('Email or password is incorrect')
            }

            const correct = await hash.compareHash(password, user.password)
            if (!correct) {
                return res.status(401).send('Email or password is incorrect')
            }
            const payload = {
                uid: user._id,
                email: user.email
            }
            const token = jwt.sign(payload, key, { expiresIn: '8h' })
            return res.status(200).send({ token })
        } catch (err) {
            console.log(err)
            res.status(500).send('oops! error')
        }
    })

    app.post('/favorites', checkToken, async(req, res) => {
        const { id } = req.body
        try {
            const movie = await Movie.findById(id).exec()
            if (!movie) {
                return res.status(400).send('No movie')
            }
            const user = await User
                .findById(req.user.uid)
                .populate('favorites')
                .exec()
            user.favorites.push(movie._id)
            await user.save()
            return res.sendStatus(200)
        } catch (err) {
            console.log(err)
           return res.status(500).send('oops! error')
        }
    });

    app.get('/favorites', checkToken, (req, res) => {
        console.log(req.user)
        const results = {
            results: []
        }
        res.json(results)
    })

    app.delete('/favorites/:id', checkToken, (req, res) => {
        const id = parseInt(req.params.id, 10)
        const index = favorites.findIndex(fav => fav.id === id)

        if (index === -1) {
            return res.sendStatus(404)
        }

        favorites.splice(index, 1)
        res.sendStatus(200)



    })

    app.listen(port, () => console.log(`started at ${port}`))
})